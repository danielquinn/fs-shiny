# fs-shiny

A visualiser for [Fat Secret](https://fatsecret.com/) data.


## Instructions

This is a simple page generation thing, so first you have generate the page,
and then you just open that page in your browser.


### Step 1: Authentication

You need to put your FatSecret username & password somewhere where *fs-shiny*
can find it, so create a file called `.env` and put your credentials in there
according to this format:

```
FS_USER='your_username'
FS_PASS='your_password'
```

### Step 2: Installing and running the generator

As the generator uses a few Python modules, you'll need a virtualenv.
Thankfully, pipenv makes that easy:

```bash
$ pipenv install
$ pipenv shell
```

Now that you're in your virtualenv, just run the generator:

```bash
$ ./generator
```

It can take a while if you've got a lot of data, but there should be some
output after about 10seconds to let you know what's going on.


### Step 3: Start a simple webserver

Viewing the stuff in a browser requires running a simple webserver in the
directory, but that's easy with python:

```bash
$ python3 -m http.server
```


### Step 4: Check it out

Open a browser and point it to http://localhost:8000/ and you should have a
chart to work with.
